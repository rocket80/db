const del = require('del');
const gulp = require('gulp');
const ts = require('gulp-typescript');

const tsProject = ts.createProject('tsconfig.json');

const jasmineOptions = {
    verbose: true,
    includeStackTrace: true,
    timeout: 5000
}

gulp.task('clean', (done) => {
    return del('target/**', done);
});

gulp.task('build', ['clean'], () => {
    return tsProject.src()
        .pipe(tsProject()).js
        .pipe(gulp.dest('target'));
});

gulp.task('default', ['build']);
