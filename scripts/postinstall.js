const execSync = require('child_process').execSync;
const path = require('path');

function execBin(what) {
    execSync(path.join('..', '.bin/', what));
}

execBin('typings install');
execBin('gulp build');
