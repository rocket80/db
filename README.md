# Database connection module

## Adding to a service

Add it to `package.json`:
```json
{
    "dependencies": {
        "db": "git+https://git@bitbucket.org/rocket80/db.git"
    }
}
```

Typings for the dependencies are not installed automatically. Add them manually to `typings.json`:
```json
{
    "dependencies": {
        "sequelize": "registry:npm/sequelize#3.0.0+20161101121248"
    },
    "globalDependencies": {
        "express": "registry:dt/express#4.0.0+20160317120654",
        "express-serve-static-core": "registry:dt/express-serve-static-core#0.0.0+20160625155614",
        "mime": "registry:dt/mime#0.0.0+20160316155526",
        "node": "registry:dt/node#6.0.0+20160621231320",
        "serve-static": "registry:dt/serve-static#0.0.0+20160606155157"
    }
}
```

## Usage

```ts
// define the database config and the models
const dbConfig: db.Config = { /* ... */ };
const models: db.Model[] = [ /* ... */ ];

// create an Orm instance
const ORM = new db.Orm(dbConfig, models);

// attach as a middleware
app.use(ORM.middleware);
```

See the template for a more complete example.

## Setting up a local working environment

The scripts in `package.json` are used when the package is installed as a dependency using
`npm install`. If you are working on this package it is nice to setup your environment.

```sh
npm install                             # install dependencies
./node_modules/.bin/typings install     # install dependency typings
./node_modules/.bin/gulp build          # compile package
```

### TODO

* Add license
* Tests
