/**
 *  Common logic for working with a database.
 *
 *  The module uses the Sequelize ORM for connecting to MySQL databases.
 *
 *  It doesn't aim to provide abstractions over Sequelize. Instead it reexports and works directly
 *  with the Sequelize types and just provides some convinience functions.
 */

export * from './orm';
export * from './error';

import * as sequelize from 'sequelize';
export { sequelize };

// Module `error` contains only an interface, so there is nothing to export in the javascript sence.
// Because of this `tsc` removes the `require('./error')` from the transpiled code.
// This is a problem for us, because there are side effects from including the module.
// To fix this we explicitly import it.
import './error';
