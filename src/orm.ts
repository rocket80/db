import { Response, NextFunction } from 'express';
import * as express from 'express';
import * as Sequelize from 'sequelize';

/**
 * Configuration for the database connection.
 */
export interface Config {
    database: string,
    username: string,
    password: string,
    host: string,
    port: string,
}

/**
 * Model definition.
 *
 * The definition is used during `Orm` construction to create a Sequelize Model.
 */
export interface ModelDef {
    /// The name of the database table this model describes.
    tableName: string,
    /// The Sequelize descriptions of the table schema.
    schema: Sequelize.DefineAttributes,
    /// Additional options used by Sequelize.
    options?: Sequelize.DefineOptions<{}>,
}

/**
 * Allow use of `Request.dep` without cast to any.
 */
interface Request extends express.Request {
    dep: any
}

/**
 * Class for connecting and communicating with the database.
 *
 * This is a thin wrapper around a Sequelize Connection.
 */
export class Orm {
    private conn: Sequelize.Connection;
    private models: Map<string, Sequelize.Model<{}, {}>>;
    private synced: Sequelize.Promise<void>;

    constructor(config: Config, modelDefs: ModelDef[]) {
        this.conn = new Sequelize(config.database, config.username, config.password, {
            dialect: 'mysql',
            host: config.host,
            port: parseInt(config.port),
        });

        this.models = new Map();
        this.model = Map.prototype.get.bind(this.models);

        modelDefs.forEach((modelDef: ModelDef) => {
            modelDef.options = modelDef.options || {};

            if (!modelDef.options.hasOwnProperty('freezeTableName')) {
                modelDef.options.freezeTableName = true;
            }

            let model = this.conn.define(modelDef.tableName, modelDef.schema, modelDef.options);
            this.models.set(modelDef.tableName, model);
        });

        this.sync();
    }

    /**
     * Get a model by its table name.
     */
    model: (tableName: string) => Sequelize.Model<{}, {}>;

    /**
     * Middleware function which attaches the ORM instance to the `express` request with `app.use`.
     */
    middleware = (req: Request, res: Response, next: NextFunction) => {
        if (this.synced.isFulfilled()) {
            req.dep.orm = this;
            next();
        } else {
            this.sync().then(() => {
                req.dep.orm = this;
                next();
            }, (err: Error) => {
                next(err);
            });
        }
    };

    /**
     * Synchronize tables with the database.
     *
     * Connects to the database and creates any tables which do not exist.
     * The sync request is cached so that it is only fired once, unless it fails.
     * Returns a promise, which is fulfilled when the sync request succeeds.
     *
     * Internally Sequelize uses a `CREATE TABLE IF NOT EXIST` query.
     * This means that it does not check if the actual schemas match.
     * To change the schema one must do a manual migration first then update the schema
     * passed to the ORM.
     */
    private sync(): Sequelize.Promise<void> {
        if (!this.synced || this.synced.isRejected()) {
            // TODO: fix the logging
            this.synced = this.conn.sync({ logging: console.log.bind(console) }).then(() => {
                console.log('[INFO] Connected to db.');
            }, (err: Error) => {
                console.log('[ERROR] DB error: ', err);
                throw err;
            });

            (this.synced as any).suppressUnhandledRejections();
        }

        return this.synced;
    }
}
