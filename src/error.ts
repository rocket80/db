import * as sequelize from 'sequelize'

/**
 * Base type for errors returned by `Sequelize`.
 */
export interface Error extends sequelize.BaseError {
    /**
     * Checks if the error is caused by database validation or something else.
     */
    isValidation: () => boolean;

    /**
     * Checks if the error is caused by a `UNIQUE` constraint on a particular field.
     */
    isUniqueFieldValidation: (field: string) => boolean;
}

(sequelize.Error.prototype as any).isValidation = function(): boolean {
    return  this instanceof sequelize.ValidationError ||
        this instanceof sequelize.ForeignKeyConstraintError ||
        this instanceof sequelize.ExclusionConstraintError;
};

(sequelize.Error.prototype as any).isUniqueFieldValidation = function(field: string): boolean {
    return this instanceof sequelize.UniqueConstraintError &&
        (this as any).fields.hasOwnProperty(field);
};
